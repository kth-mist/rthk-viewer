import './Info.css'
import Dropdown from 'react-bootstrap/Dropdown';
import 'chartjs-plugin-streaming'
import { Chart as ChartJS, LinearScale, CategoryScale, RadialLinearScale, BarElement, PointElement, LineElement, ArcElement, Legend, Tooltip} from 'chart.js'
import {Line} from 'react-chartjs-2'
import { useState, useEffect } from "react"
ChartJS.register(
	LinearScale,
	RadialLinearScale,
	CategoryScale,
	BarElement,
	PointElement,
	LineElement,
	ArcElement,
	Legend,
	Tooltip
)
export default function Info({ hks }) {

	const [hk, setHK] = useState({})
	
	const updateHK = () => {
		fetch('http://localhost:8008')
			.then((response) => response.json())
			.then((data) => {
				let temp = {}

				data.housekeeping.forEach((x) => {
					if (x != {})
						temp[Object.keys(x)[0]] = x[Object.keys(x)[0]]
				})
				
				setHK(temp)
			})
	}

	useEffect(() => {
		updateHK()
		const interval = setInterval(() => {
			updateHK()
		}, 2000);
		return () => clearInterval(interval);
	}, []);

	return (
			<div className="container-fluid" style={{height: "100%", padding: "0 70px 20px 70px"}}>
			<div className="row">
			<div className="col-2 info-card">
			<p style={{fontWeight: "bold"}}>
			Battery
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Voltage:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Current out:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Temperature 1:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Temperature 2:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Heater:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Switch:
		{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_VBATT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Battery mode:
		{ typeof hk['HK_EPS_BATTMODE'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_BATTMODE'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			</ul>
			</div>

			<div className="col-3 info-card">
			<p style={{fontWeight: "bold"}}>
			Solar panel power
		</p>

			<table style={{border: 0}}>
			<tr>
			<td colspan="2" style={{textAlign: "left"}}>Power tracking mode:</td>
			<td>
			{ typeof hk['HK_EPS_VBATT'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_VBATT'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<br/>
			<tr>
			<td></td>
			<td>mV</td>
			<td>mA</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Boost conv 0:</td>
			<td>
			{ typeof hk['HK_EPS_VBOOST_0'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_VBOOST_0'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CURIN_0'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CURIN_0'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Boost conv 1:</td>
			<td>
			{ typeof hk['HK_EPS_VBOOST_1'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_VBOOST_1'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CURIN_1'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CURIN_1'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Boost conv 2:</td>
			<td>
			{ typeof hk['HK_EPS_VBOOST_2'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_VBOOST_2'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CURIN_2'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CURIN_2'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>All solar panels:</td>
			<td>
			</td>
			<td>
			{ typeof hk['HK_EPS_CURSYS'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CURSYS'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			</table>			
			</div>
			<div className="col-3 info-card">
			<p style={{fontWeight: "bold"}}>
			EPS switched outputs
		</p>
			<table style={{border: 0}}>
			<tr>
			<td></td>
			<td>mV</td>
			<td>mA</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 0:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_0'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_0'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_0'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_0'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 1:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_1'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_1'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_1'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_1'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 2:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_2'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_2'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_2'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_2'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 3:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_3'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_3'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_3'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_3'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 4:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_4'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_4'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_4'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_4'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>Output 5:</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_5'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_5'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_5'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_5'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>3.3V</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_5'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_5'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			</td>
			</tr>
			<tr>
			<td style={{textAlign: "left"}}>5.0V</td>
			<td>
			{ typeof hk['HK_EPS_CUROUT_5'] != 'undefined' ?
				<span style={{float: "right", textAlign: "right"}}>
				{hk['HK_EPS_CUROUT_5'].value.toFixed(2)}
				</span>
				:null
			}
		</td>
			<td>
			</td>
			</tr>

		</table>			
			</div>
			<div className="col-2 info-card">
			<p style={{fontWeight: "bold"}}>
			EPS watchdogs, reboots and temps
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Counter WDT I2C:
		{ typeof hk['HK_EPS_COUNTER_WDT_I2C'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_COUNTER_WDT_I2C'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Counter WDT GND:
		{ typeof hk['HK_EPS_COUNTER_WDT_GND'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_COUNTER_WDT_GND'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			EPS reboot counter:
		{ typeof hk['HK_EPS_COUNTER_BOOT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_COUNTER_BOOT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			EPS Temp 0:
		{ typeof hk['HK_EPS_TEMP_0'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_TEMP_0'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			EPS Temp 1:
		{ typeof hk['HK_EPS_TEMP_1'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_TEMP_1'].value.toFixed(2)}
			</span>
			:null
		}
		</li>

			<li>
			EPS Temp 2:
		{ typeof hk['HK_EPS_TEMP_2'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_TEMP_2'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			EPS Temp 3:
		{ typeof hk['HK_EPS_TEMP_3'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_EPS_TEMP_3'].value.toFixed(2)}
			</span>
			:null
		}
		</li>


		</ul>
			
		</div>
			<div className="col-2 info-card">
			<p style={{fontWeight: "bold"}}>
			Transmitter
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Forward Pwr:
		{ typeof hk['HK_TRXVU_TX_FWRDPWR'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_TX_FWRDPWR'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Reflected Pwr:
		{ typeof hk['HK_TRXVU_TX_REFLPWR'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_TX_REFLPWR'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			PA Temp:
		{ typeof hk['HK_TRXVU_TX_PA_TEMP'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_TX_PA_TEMP'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			PA Voltage:
		{ typeof hk['HK_TRXVU_TX_BUS_VOLT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_TX_BUS_VOLT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			PA Current:
		{ typeof hk['HK_TRXVU_TX_CURRENT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_TX_CURRENT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
		</ul>
			
		</div>
			<div className="col-2 info-card">
			<p style={{fontWeight: "bold"}}>
			Receiver
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Doppler:
		{ typeof hk['HK_TRXVU_RX_DOPPLER'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_RX_DOPPLER'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			RSSI:
		{ typeof hk['HK_TRXVU_RX_RSSI'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_RX_RSSI'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Local osc temp:
		{ typeof hk['HK_TRXVU_RX_LOCOSC_TEMP'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_RX_LOCOSC_TEMP'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			RX Voltage:
		{ typeof hk['HK_TRXVU_RX_BUS_VOLT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_RX_BUS_VOLT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			RX Current:
		{ typeof hk['HK_TRXVU_RX_CURRENT'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_TRXVU_RX_CURRENT'].value.toFixed(2)}
			</span>
			:null
		}
		</li>

		</ul>
			
		</div>
			<div className="col-2 info-card">
			<p style={{fontWeight: "bold"}}>
			On-board computer
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Operation Mode:
		{ typeof hk['HK_OBCSW_OP_MODE'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_OBCSW_OP_MODE'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			TC Queue Length:
		{ typeof hk['HK_OBCSW_TC_QUEUE_LENGTH'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_OBCSW_TC_QUEUE_LENGTH'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Reset Counter:
		{ typeof hk['HK_OBCSW_RESET_COUNTER'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_OBCSW_RESET_COUNTER'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			iOBC Temp:
		{ typeof hk['HK_OBCSW_IOBC_TEMPERATURE'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_OBCSW_IOBC_TEMPERATURE'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
		</ul>
			
		</div>
			<div className="col-3 info-card">
			<p style={{fontWeight: "bold"}}>
			ADCS
		</p>
			<ul style={{paddingInlineStart: 0, listStyle: 'none', textAlign: "left"}}>
			<li>
			Raw magnetometer X:
		{ typeof hk['HK_ADCS_RawMTMData3_RAW_Mag_X'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_RawMTMData3_RAW_Mag_X'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
						<li>
			Raw magnetometer Y:
		{ typeof hk['HK_ADCS_RawMTMData4_RAW_Mag_Y'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_RawMTMData4_RAW_Mag_Y'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Raw magnetometer Z:
		{ typeof hk['HK_ADCS_RawMTMData5_RAW_Mag_Z'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_RawMTMData5_RAW_Mag_Z'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Current Coil X:
		{ typeof hk['HK_ADCS_CoilCurrent3_Coil_X_Current'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_CoilCurrent3_Coil_X_Current'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Current Coil Y:
		{ typeof hk['HK_ADCS_CoilCurrent4_Coil_Y_Current'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_CoilCurrent4_Coil_Y_Current'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			Current Coil Z:
		{ typeof hk['HK_ADCS_CoilCurrent5_Coil_Z_Current'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_CoilCurrent5_Coil_Z_Current'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
			<li>
			MCU Temperature:
		{ typeof hk['HK_ADCS_ENG_HK_DATA13_MCU_Temperature'] != 'undefined' ?
			<span style={{float: "right", textAlign: "right"}}>
			{hk['HK_ADCS_ENG_HK_DATA13_MCU_Temperature'].value.toFixed(2)}
			</span>
			:null
		}
		</li>
		</ul>
			
		</div>
			
		</div>
			</div>
	)
}
