import './PlotCard.css'
import Dropdown from 'react-bootstrap/Dropdown';
import 'chartjs-plugin-streaming'
import { Chart as ChartJS, LinearScale, CategoryScale, RadialLinearScale, BarElement, PointElement, LineElement, ArcElement, Legend, Tooltip} from 'chart.js'
import {Line} from 'react-chartjs-2'
import { useState, useEffect } from "react"
ChartJS.register(
	LinearScale,
	RadialLinearScale,
	CategoryScale,
	BarElement,
	PointElement,
	LineElement,
	ArcElement,
	Legend,
	Tooltip
)
export default function PlotCard({ id, hks }) {

	const [ selectedSID, setSelectedSID ] = useState("Select SID")
	const [ selectedUNIT, setSelectedUNIT ] = useState("Select UNIT")
	const [ selectedHK, setSelectedHK ] = useState({hk: "Select HK", index: -1})
	const [ stateHK, setStateHK ] = useState([])

	const [ responseData, setResponseData ] = useState([])

	const compareArrays = (a, b) => {
		return JSON.stringify(a) === JSON.stringify(b);
	};

	let SIDs = null;
	let UNITs = null;
	let HKs = null;
	var housekeeping = null;

	const [updatedValue, setUpdatedValue] = useState({
		datasets :
		[
		]
	})


	const getSIDs = () => {
		let temp = []
		temp.push("ALL")

		hks.housekeeping.forEach((hk, id) => {
			for(var key in hk) {
				temp.push(key.split("_")[1])
			}
		})
		
		SIDs = Array.from(new Set(temp))

		getUNITs()

	}

	const getUNITs = () => {
		let temp = []
		temp.push("ALL")

		hks.housekeeping.forEach((hk, id) => {
			for(var key in hk) {
				if(selectedSID === "ALL") {
					temp.push(hk[key].unit)
				} else if(key.split("_")[1] === selectedSID) {
					temp.push(hk[key].unit)
				}
			}
		})
		UNITs = Array.from(new Set(temp))
		getHKs()
	}

	const getHKs = () => {
		let temp = []
		temp.push({hk: "ALL", index: -1})

		hks.housekeeping.forEach((hk, id) => {
			for(var key in hk) {
				if(selectedSID === "ALL" && selectedUNIT === "ALL") {
					temp.push({hk: key, index: id})
				} else if(selectedSID === "ALL" && hk[key].unit === selectedUNIT) {
					temp.push({hk: key, index: id})
				} else if(selectedSID === key.split("_")[1] && selectedUNIT === "ALL") {
					temp.push({hk: key, index: id})
				} else if(selectedSID === key.split("_")[1] && selectedUNIT === hk[key].unit) {
					temp.push({hk: key, index: id})
				}
			}
		})
		HKs = Array.from(new Set(temp))

		if(selectedHK.hk === "ALL") {
			let temp = new Set(HKs)
			temp.forEach((el) => {
				if(el.hk === "ALL") {
					temp.delete(el);
				}})
			housekeeping = Array.from(temp)
		} else if (selectedHK.hk === "Select HK"){
			housekeeping = []
		} else {
			housekeeping = [selectedHK]
		}

		if(!compareArrays(stateHK, housekeeping)){
			setStateHK(housekeeping)
			let updatedValueTemp = { datasets : []}
			housekeeping.forEach(x => {
				var randomColor = "#" + Math.floor(Math.random()*16777215).toString(16);
				updatedValueTemp.datasets.push({
					label : x.hk,
					data : [],
					showLine: true,
					backgroundColor: randomColor,
					borderColor: randomColor
				})
			})
			setUpdatedValue(updatedValueTemp)
		}

	}

	getSIDs()

	const updateHK = () => {
		fetch('http://localhost:8008')
			.then((response) => response.json())
			.then((data) => {
				let temp = updatedValue;

				setResponseData(data)
				
				stateHK.forEach((hk, i) => {

					if(temp.datasets[i].data.length > 10) {
						temp.datasets[i].data.shift()
					}

					temp.datasets[i].data.push({
						x: Date.now(),
						y: data.housekeeping[hk.index][hk.hk].value
					})
				})

				setUpdatedValue(temp)

			})

		
	}

	useEffect(() => {
		updateHK()
		const interval = setInterval(() => {
			updateHK()
		}, 2000);
		return () => clearInterval(interval);
	}, [stateHK]);



	var commonOptions = 
		{
			animation: {
				duration: 0
			},
            plugins: {
                streaming: {
                    frameRate: 30,
                }
            },
            scales: {
                x: {
                    type: 'linear'
                }
            }
        }
    ;

	return (
		<div className="container-fluid" style={{height: "80%"}}>
			<div className="row">
				<div className="col-4">
					<Dropdown>
						<Dropdown.Toggle id="dropdown-basic">{selectedSID}</Dropdown.Toggle>
						<Dropdown.Menu>
							{SIDs.map((sid, id) => {
								return (<Dropdown.Item key={id} as="button" onClick={() => setSelectedSID(sid)}>{sid}</Dropdown.Item>)
							})}
						</Dropdown.Menu>
					</Dropdown>
				</div>

				<div className="col-4">
					<Dropdown>
						<Dropdown.Toggle id="dropdown-basic-2">{selectedUNIT}</Dropdown.Toggle>
						<Dropdown.Menu>
							{UNITs.map((unit, id) => {
								return (<Dropdown.Item key={id} as="button" onClick={() => setSelectedUNIT(unit)}>{unit}</Dropdown.Item>)
							})}
						</Dropdown.Menu>
					</Dropdown>
				</div>

				<div className="col-4">
					<Dropdown>
						<Dropdown.Toggle id="dropdown-basic-3">{selectedHK.hk}</Dropdown.Toggle>
						<Dropdown.Menu>
							{HKs.map((housekeeping, id) => {
								return (<Dropdown.Item key={id} as="button" onClick={() => setSelectedHK({hk: housekeeping.hk, index: housekeeping.index})}>{housekeeping.hk}</Dropdown.Item>)
							})}
						</Dropdown.Menu>
					</Dropdown>
				</div>

				<div className="col-12">
					<p style={{height: "100%"}}>
						{housekeeping  && <Line style={{margin: "40px 0 0 0"}} data={updatedValue} options={commonOptions}/>}
					</p>

				</div>	
			</div>
		</div>
	)
}
