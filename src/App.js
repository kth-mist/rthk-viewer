import logo from './logo.svg';
import './App.css';
import Navbar from './components/Navbar'
import PlotCardList from './components/PlotCardList'
import { useEffect } from "react"

function App() {
	
	return (
			<div className="App">
			<Navbar />
			<div className="container-fluid">
			<PlotCardList />
			</div>
			</div>
	);
}

export default App;

