import './PlotCardList.js'
import PlotCard from './PlotCard'
import Info from './Info'
import { useState } from "react"

export default function PlotCardList() {

	const [graphComponents, setGraphComponents] = useState([])
	const [listSize, setListSize] = useState(0)
	const [fetchedData, setFetchedData] = useState(false)

	const [responseData, setResponseData] = useState([])

	const handleNew = () => {
		setListSize( listSize + 1)
		let tempArr = graphComponents;
		tempArr.push(listSize)
		setGraphComponents(tempArr);
	}

	const handleRemove = (id) => {
		let tempArr = graphComponents;
		const index = tempArr.indexOf(id);
		if (index > -1) {
			tempArr.splice(index, 1);
			setListSize( listSize + 1)
			setGraphComponents(tempArr);
		}	
	}

	const updateHK = () => {
		fetch('http://localhost:8008')
			.then((response) => response.json())
			.then((data) => setResponseData(data))
	}

	if(!fetchedData) {
		updateHK()
		setFetchedData(true)
	}

	return (
			<div className="row">
			<Info hks={responseData}/>
			{graphComponents.map( (id) => {
				return (
						<div key={id} className="col-12 card-height" style={{padding: "10px 20px"}}>
						<button style={{background: "red", color: "white", margin: "0 0 30px 0", borderRadius: "10%"}} onClick={() => handleRemove(id)}> Remove graph </button>
						<PlotCard id={id} hks={responseData}/>
						</div>
				);
			})}
			<div className="col-12" style={{padding: "10px 20px"}}>
			<button className="add-new-graph" onClick={handleNew}> Add new </button>
			</div>
			</div>
	)
}
