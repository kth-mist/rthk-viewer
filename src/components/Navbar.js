import './Navbar.css'
import Logo from '../assets/logo.jpeg'

export default function Navbar() {
return (
<div className='navbar-mist'>
  <img src={Logo} alt="logo"/> <em className="title"> MIST - Dashboard</em>
</div>
)
}
